# Gestion de configuration

Dans ce depôt git seront hébergées toutes les ressources liées au cours de Gestion de Configuration 2022-2023.

## [Cours](./cours/README.md)

- [Intro Gestion de Configuration](./cours/intro/README.md)
- [Ansible](./cours/ansible/README.md)

## [TP](./tp/README.md)

- [TP1 : Vagrant et Ansible](./tp/1/README.md)
- [TP2 : Ansible et structure de dépôt](./tp/2/README.md)
- [TP3 : First boot conf](./tp/3/README.md)
- [TP4 : Prise en main AWX](./tp/4/README.md)
- [TP5 : Conformité et CI/CD](./tp/5/README.md)
- [TP6 : Your own stack](./tp/6/README.md)
