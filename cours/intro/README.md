# Intro Gestion de Configuration

Dans ce premier cours on va présenter un peu la notion de **Gestion de Configuration**.

## 1. Présentation générale

**La gestion de configuration est le fait de stocker dans des fichiers texte les configurations des différentes machines d'un parc.**

**Ces fichiers texte sont utilisés par des applications adéquates afin de déployer automatiquement une configuration donnée sur une cible donnée.**

Cette démarche a plusieurs objectifs :

- **centraliser les configurations dans un endroit unique**
  - généralement, dans un (ou plusieurs) dépôt(s) git
- **permettre la réutilisation des configurations**
  - écrire les configs une fois, les déployer plein de fois

Les bénéfices d'une telle démarche sont multiples :

- stabilité, répétabilité et **automatisation** des déploiements
- facilité pour déployer une **politique de sécurité** sur les machines
- **garder la complexité sous contrôle**
- **réduction des risques** et des coûts liés à la croissance de l'infrastructure et de sa complexité

Toutes ces configurations facilement accessibles dans un endroit centralisé, il devient alors possible, en une seule commande, de :

- déployer ponctuellement une machine, un serveur, à la demande
- déployer ponctuellement une config spécifique sur une ou plusieurs machines

> On dit ici "en une seule commande" mais il existe évidemment de jolies GUI qui permettent d'encadrer de tels environnements. Nous en verrons pendant le cours.

## 2. Quelles configurations

**Toutes !**. Avec les technologies d'aujourd'hui, il est possible d'avoir des outils à tous les niveaux d'infrastructure.

On pense donc aux configurations suivantes :

- **déploiement de machines virtuelles**
  - ici on pense à des outils comme Vagrant ou Terraform
- **configuration initiale** d'une machine fraîchement créée
  - installer un OS dans notre VM, c'est pas mal
  - PXE ou utilisation d'une plateforme cloud
- **déploiement de la configuration système** initiale
  - Kickstart, Preseed, cloud-init
- **déploiement de configurations additionnelles**
  - Ansible, Puppet, autres
- **déploiement applicatif**
  - docker, Kubernetes, autres

> Nous n'aborderons pas l'aspect physique dans ce cours, et supposerons l'installation préalable de serveurs physiques, munis d'un hyperviseur.

## 3. Versioning

Il est courant d'utiliser **un outil de versioning comme des dépôts git afin de stocker toutes ces configurations**.

L'utilisation de fichier texte permet de gérer ces fichiers de configurations comme du code, et donc de bénéficier des avantages liés au versioning :

- travail collaboratif
- conserver un historique complet de tout le code produit
- accès à toutes les configs de façon unifiée et sécurisée
- gestion de branche (environnements)

## 4. Limites de la gestion de config

La gestion de config permet d'automiser le déploiement de tout un parc informatique, des VMs mais présentent malgré tout quelques inconvénients :

➜ certains outils sont **fortement couplés** à certains environnements

- on pense en particulier aux outils spécifiques à certains OS pour les installations réseau

➜ la **révocation/annulation n'est pas native**

- il n'existe pas de commande magique pour rollback un déploiement donné
- il faut souvent coder un deuxième fichier qui ordonnera la suppression de ce qui a été réalisé

➜ le **contrôle de conformité est exclu**

- la conformité est une part importante de la gestion d'infra et est parfois confondue avec le déploiement automatisé
- la conformité permet de vérifier qu'une ou plusieurs machines sont bien dans l'état voulu
- déployer un truc c'est bien, vérifier qu'il y a pas un cryptominer juste à côté, c'est mieux
