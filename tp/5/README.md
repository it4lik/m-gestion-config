# TP5 : Conformité et CI/CD

Dans ce TP on va compléter la boîte à outils autour de la gestion de conf avec deux derniers ajouts :

- **contrôle de conformité avec OpenSCAP**
  - le contrôle de conformité c'est l'idée de vérifier qu'une machine est bien dans l'état que l'on souhaite
  - OpenSCAP est un outil qui est livré avec des politiques pré-écrites
  - notamment des politiques avancées pour la sécurité
- **CI/CD avec Gitlab**
  - on va pousser notre code dans un dépôt git dédié
  - lorsqu'on poussera du nouveau code Ansible, une pipeline sera déclenchée, et le nouveau code sera déployé

## Sommaire

- [TP5 : Conformité et CI/CD](#tp5--conformité-et-cicd)
  - [Sommaire](#sommaire)
- [0. Setup](#0-setup)
- [I. Conformité](#i-conformité)
  - [1. Premiers pas](#1-premiers-pas)
  - [2. Remédiation avec Ansible](#2-remédiation-avec-ansible)
- [II. CI et CD](#ii-ci-et-cd)
  - [1. Le lab](#1-le-lab)
  - [2. Premiers pas CI](#2-premiers-pas-ci)
  - [3. Tests de syntaxe](#3-tests-de-syntaxe)
  - [4. Déploiement automatique](#4-déploiement-automatique)
    - [A. Clé SSH](#a-clé-ssh)
    - [B. ngrok](#b-ngrok)
    - [C. Deploy](#c-deploy)

# 0. Setup

➜ Pour ce TP, **créez un nouveau dépôt Git dédié**, cela facilitera la tâche pour pas mal d'étapes.

- créez-le sur la plateforme [**Gitlab**](https://gitlab.com/), afin de profiter des fonctionnalités de CI/CD qu'elle offre
- ce sera votre répertoire de travail pour tout le TP

➜ Pour tout le TP, dès qu'on a besoin de VM, **on utilisera les Rocky 9 qu'on a packagé au TP3 avec Vagrant**

- c'est déjà préconfiguré
- et prêt à accueillir de la conf Ansible
- créez donc un `Vagrantfile` à la racine du dépôt git
- il déploie une seule machine

➜ **Mettre en place le dépôt Ansible**

- créer le fichier `ansible.cfg`
- créer un dossier `roles/`
- créer un dossier `playbooks/`
- créer un dossier `inventories` qui contiendra un inventaire pour déployer de la configuration sur l'unique machine déclarée dans le `Vagrantfile`

# I. Conformité

## 1. Premiers pas

➜ **Lancer un machine virtuelle** avec Vagrant

- on va travailler manuellement dans un premier temps
- une fois la VM allumée, connectez-vous en SSH

➜ **Installez les paquets suivants** :

- `openscap`
- `openscap-scanner`
- `openscap-utils`
- `scap-security-guide`

➜ Il existe des règles de conformité pré-écrites et à notre disposition librement

- choisissez un profil (un jeu de règles) que vous voulez tester
- j'ai testé pour ce cours le profil de l'ANSSI niveau *intermediate*
- pour avoir la liste des profils disponibles, exécutez la commande suivante

```bash
# rl9 pour Rocky Linux 9, les règles sont adaptées à notre OS
oscap info /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
```

➜ **Effectuez un contrôle de conformité** sur la machine

- on va utiliser une commande qui produit un rapport au format HTML
- utilisez la commande suivante

```bash
# la commande produira un fichier report.html dans le dossier courant
sudo oscap xccdf eval --report report.html --profile <PROFIL_CHOISI> /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml
```

- vous pouvez récupérez le rapport `report.html` sur votre machine et le visualiser dans votre navigateur web
- baladez-vous un peu, observez ce qui a été détecté
- depuis cette interfacen on peut extraire des playbooks Ansible de remédiation déjà

## 2. Remédiation avec Ansible

Pour la remédiation, on va pas non plus à chaque fois passer par un rapport HTML.

Il est possible de générer un *playbook* Ansible complet de remédiation.

➜ **Pour ce faire :**

```bash
# production d'un rapport de contrôle de conformité
oscap xccdf eval --profile <PROFIL_CHOISI> --results report.xml /usr/share/xml/scap/ssg/content/ssg-rl9-ds.xml

# production du playbook Ansible de remédiation
# cette commande produira un playbook Ansible remediation.yml dans le dossier courant
oscap xccdf generate fix --fix-type ansible --profile <PROFIL_CHOISI> --output remediation.yml report.xml
```

➜ **Intégrer le playbook de remédiation à votre dépôt Ansible**

- récupérer le fichier `remediation.yml` et placez-le dans le dossier `playbooks/`
- déroulez le *playbook* avec une commande `ansible-playbook` !
- il sera nécessaire d'adapter deux-trois ptits trucs pour que ça déroule mais ça se fait bien :)
- n'hésitez pas à `destroy` puis re-up la machine pour les tests !

> On pourrait imaginer un rôle Ansible `openscap` qui serait chargé de produire automatiquement ces *playbooks* de remédiation qui sont amenés à évcluer dans le temps au fur et à mesure des mises à jour et de la diversité des environnements mis en place.

# II. CI et CD

## 1. Le lab

On va utiliser l'instance publique de Gitlab pour mettre en place la CI/CD, ça nous évitera la partie fastidieuse de monter nous-mêmes un tel environnement pour se concentrer sur la partie gestion de config.

Gitlab est nativement muni du nécessaire pour effectuer des tâches lorsque du code est poussé sur le dépôt Git.

On appelle *pipeline* un job qui est déclenché quand du code est poussé sur le dépôt Git.

Une *pipeline* est une suite d'action, qui s'exécute dans un environnement donné.

Chaque *pipeline* s'exécute sur un *Runner* : c'est une machine dédiée à l'exécution de *pipelines*. Quand du code est poussé, le *Runner* récupère le contenu du dépôt Git afin d'opérer des actions.

Dans le cadre du TP on va mettre en place deux choses :

- test de syntaxe et *linting* du code Ansible automatisé
  - à chaque `git push`, si du nouveau code Ansible est poussé, il sera testé
- déploiement automatique sur la VM Vagrant
  - si du code Ansible est poussé, on déclenche automatiquement l'exécution d'un *playbook*

## 2. Premiers pas CI

➜ **Créez une première *pipeline***

- créer un fichier `.gitlab-ci.yml` à la racine de votre dépôt
- déposez le contenu suivant :

```yml
image: alpine

stages:
  - first-steps

first-steps:
  stage: first-steps
  script:
    - echo test
```

> Notez l'utilisation du mot-clé `image`. En effet, le *Runner* Gitlab qui exécuter notre *pipeline* va lancer un conteneur Docker afin d'exécuter les commandes qu'on demande.

➜ **Rendez-vous sur la WebUI de Gitlab**

- sur la page de votre dépôt, dans le menu latéral, allez dans le menu `CI/CD`
- vous devriez voir votre job en cours d'exécution

## 3. Tests de syntaxe

➜ **Mettons en place les tests de syntaxe sur le code Ansible**

- adaptez le `gitlab-ci.yml` :

```yml
image: python:3-slim

before_script:
  - apt update -y
  - apt install -y git
  - pip install ansible ansible-lint

stages:
  - ansible-lint

ansible-lint:
  stage: ansible-lint
  script:
    - ansible-lint playbooks/*
```

> Notez l'utilisation du `before_script` qui va préparer l'environnement avant l'exécution des commandes de chaque *stage* de la *pipeline*.

➜ **Condition !**

- adaptez le `gitlab-ci.yml` pour que ce stage de *linting* ne se déclenche QUE si des fichiers `.yml` sont push

## 4. Déploiement automatique

Pour le déploiement automatique l'idée est la suivante :

- vous écrivez du code Ansible
- vous faites un `git push`
- le code est réceptionné sur le serveur git
- une *pipeline* est déclenchée
- cette *pipeline* exécute une commande `ansible-playbook` pour déployer du code sur notre machine Vagrant

Pour que ça fonctionne deux choses :

- un truc normal, et on va faire ça clean :
  - faire en sorte que la *pipeline* ait accès a une clé SSH qui peut se connecter à notre machine
  - on va générer une paire de clé dédiée
  - et utiliser une *variable masquée* de CI pour l'aspect sécu
- un truc moins normal : rendre la VM accessible en SSH sur internet
  - bah ui il faut bien que Gitlab puisse s'y connecter pour déployer la conf !
  - on va faire ça avec `ngrok` un outil bien pratique et utile à toujours avoir sous la main si vous ne connaissez pas

### A. Clé SSH

➜ **Générer une paire de clé SSH**

- ne les placez pas dans le dépôt git, mettez-les ailleurs n'importe tout sur votre machine, facilement accessible
- afin de pouvoir la *masquer* avec Gitlab, pour pas qu'elle soit visible pendant l'exécution des *pipelines* il faudra l'encoder en base64
- avec les commande :

```bash
# génération de la paire de clé
# choisissez une chemin facilement accessible pour cette nouvelle paire de clés
ssh-keygen -t rsa -b 4096

# encodage de la clé en base64
cat /path/to/id_rsa | base64 -w0
```

➜ **Utiliser Ansible pour déployer la clé**

- déposez la clé publique `id_rsa.pub` sur un utilisateur de la machine qui a accès aux droits `sudo`
  - en vrai, restons simple pour le lab : utilisez l'utilisateur `vagrant` non ?
- il existe un module Ansible dédié pour les clés SSH je vous laisse faire une recherche

➜ **Rendez-vous sur la WebUI de Gitlab**

- sur la page de projet
- menu latéral : `Settings > CI/CD`
- dépliez le menu `Variables` et ajoutez une nouvelle variable
  - appelez-la `SSH_PRIVATE_KEY`
  - sa valeur est la string base64 calculée précédemment

### B. ngrok

`ngrok` est un outil **bien pratique**, **mais à bannir complètement dans un contexte réel de production**. Il va juste nous être utile ici pour rendre accessible en SSH notre VM depuis internet.

L'idée est simple : on lance une commande `ngrok` en indiquant le port qu'on veut partager (port 22 en TCP en l'occurence pour nous, pour SSH), et un des serveurs de `ngrok` met gracieusement en place un reverse shell.

Autrement dit, **le serveur SSH de notre VM locale sera accessible sur une adresse joignable publiquement**.

> `ngrok` c'est très pratique et très cool mais aussi très pratique pour des utilisations frauduleuses. Ils ont donc mis en place un système de compte et de token afin d'éviter les abus. Il sera donc nécessaire de vous créer un compte. Hésitez pas si vous avez besoin d'aide pour ce token.

---

➜ **Connectez-vous à la VM Vagrant depuis une adresse publique**

- installez `ngrok` en suivant les instructions du site officiel
- exécutez `ngrok tcp 22`
- vous obtenez une adresse et un port joignable depuis internet
- testez que vous pouvez correctement vous y connecter
  - utilisez l'utilisateur et la clé SSH générée précédemment

### C. Deploy

➜ **Testez qu'une *pipeline* peut se connecter à votre VM Vagrant**

- utilisez un fichier `.ssh-config` comme aux autres TP pour enregistrer les informations nécessaires
  - il faut le push dans le dépôt git, à la racine
  - le contenu par exemple :

```
Host node1_tp5
  HostName 5.tcp.eu.ngrok.io
  User vagrant
  Port 12714
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentitiesOnly yes
  LogLevel FATAL
```

➜ **Adaptez le `.gitlab-ci.yml`**

```yml
image: cytopia/ansible

before_script:
  - apk update
  - apk add openssh-client
  - mkdir ~/.ssh
  - echo "$SSH_PRIVATE_KEY" | base64 -d > ~/.ssh/id_rsa
  - chmod 0700 ~/.ssh
  - chmod 0400 ~/.ssh/id_rsa
  - cat .ssh-config > ~/.ssh/config

stages:
  - test

test:
  stage: test
  script:
    - ssh node1_tp5 echo hello
```

➜ **Adaptez le `.gitlab-ci.yml`**

- pour qu'il exécute une commande `ansible-playbook`
- lorsqu'un fichier `.yml` est modifié
