# TP6 : Your own stack

Dernier TP un peu plus libre pour finir les 3 dernières séances.

➜ Le but : **réutiliser les outils de gestion de config qu'on a vu en cours pour déployer une stack complète.**  
Je vous laisse libre le choix de la solution sur laquelle partir, voyez large (déployer un cluster Kube, OpenNebula, Openstack, CEPH, une stack applicative, etc).

➜ **Le rendu attendu est un dépôt git**, qui contient :

- **un `README.md`**
  - instructions pour construire une image de base
    - si elles diffèrent de ce qu'on a fait en cours
  - explications pour lancer la stack (quel(s) playbook(s) Ansible lancer par exemple)
- **un `Vagrantfile`**
  - met en place un environnement virtuel
  - avec des VMs utilisant une image custom
- **`cloud-init`** sera utilisé
- **un dépôt Ansible**
  - un dépôt propre qui met en place une structure inventaire/roles
- **`.gitlab-ci.yml`** ou équivalent
  - qui déploie la nouvelle conf automatiquement
  - pourquoi pas ajouter `ngrok` à vos machines de base pour faciliter les tests

➜ Ca ressemble beaucoup à ce qu'on pourrait fournir pour fournir un environnement de dév/test complet sur une techno donnée

- tout le nécessaire pour déployer
- ressemble beaucoup à ce qu'on aurait en production, mais sans les soucis de mise à l'échelle

➜ Quelques idées pour aller plus loin sur le plan technique :

- tester Packer pour créer votre image de base
- quelques techniques Ansible supplémentaire :
  - inventaire dynamique
  - vault ansible
  - utilisation de tags
- intégrer une gestion de conteneurs Docker avec Ansible