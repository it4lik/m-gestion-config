# TP4 : Prise en main AWX

**Petit TP pour prend la main sur AWX.**

AWX est un outil qui permet essentiellement de :

- fournir une interface graphique par dessus Ansible
- déclencher des jobs Ansible via API
- gérer des permissions d'accès aux playbooks
- générer des logs d'exécution

> Assurez vous d'avoir un dépôt Ansible fonctionnel pour la réaliser ce TP (TP2) et de quoi provisionner facilement des hôtes avec de quoi déployer Ansible (TP3).

## Sommaire

- [TP4 : Prise en main AWX](#tp4--prise-en-main-awx)
  - [Sommaire](#sommaire)
  - [0. Setup](#0-setup)
  - [I. AWX](#i-awx)
    - [1. Concepts](#1-concepts)
    - [2. Intégrer notre code Ansible](#2-intégrer-notre-code-ansible)

## 0. Setup

On va suivre la doc pour installer AWX. Les deux méthodes d'install recommandées sont : Kubernetes ou `docker-compose`. On va se tourner vers [l'install avec `docker-compose`](https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md).

Il sera juste nécessaire de modifier une ligne dans le template `docker-compose` car par défaut, le conteneux AWX va essayer d'écouter sur le port `2222/tcp`.  
Hors, nous, on a déjà Vagrant sur ce port.

Ca se passe dans le fichier `./tools/docker-compose/ansible/roles/sources/templates/docker-compose.yml.j2` dans le dépôt AWX. Cherchez la ligne avec un partage de port et éditez-la :

```j2
[...]
      - "22:2222"  # receptor foo node
[...]
```

Une fois la stack allumée, l'interface d'AWX est disponible sur `https://127.0.0.1:8043`.

N'oubliez pas de "build l'UI" ([cette section de la doc](https://github.com/ansible/awx/blob/devel/tools/docker-compose/README.md#clean-and-build-the-ui)). Bah ouais c'est Docker : build it, ship it, run it aaaaand rebuild it ?

## I. AWX

### 1. Concepts

AWX offre une surcouche à Ansible et amène un modèle complet afin de permettre l'interaction avec le code Ansible.

Ainsi, pour déployer un *playbook* il faut :

➜ avoir **un user** qui a des droits suffisants

- la doc vous indique comment créer un user admin

➜ **créer un *Project***

- c'est un lien vers un dépôt git qui contient du code Ansible
- il sera nécessaire de le refresh si du nouveau code Ansible est push sur le dépôt git

➜  **créer des *Credentials***

- c'est un ensemble de données que peut utiliser AWX pour se connecter à des hôtes distants avec Ansible
- une connexion SSH donc.

➜  **créer un *Inventory***

- similaire à la notion d'inventaire dans Ansible : une liste d'hôte
- on pourra importer directement l'inventaire Ansible existant
- il faudra indiquer une Source : un fichier d'inventaire Ansible trouvé dans le dépôt git

➜  **enfin, créer un *Template***

- ça correspond à l'exécution d'un playbook Ansible
- on exécute un playbook trouvé dans le Project
- AWX utiliser les Credentials pour qu'Ansible se connecte aux machine à distance
- en se basant sur l'Inventory

Si les étapes semblent pénible pour exécuter un simple *playbook* c'est normal : AWX n'est pas là pour l'exécution simple de *playbook*, il y a la ligne de commande pour ça.

Pour le développement de code Ansible, la ligne de commande n'a pas son pareil. En revanche, quand il s'agit *d'ancrer les interactions qu'on a avec Ansible et son impact sur un parc informatique, AWX est un outil de choix.**

Ce modèle (*Project*, *Template*, *Credentials*, etc.) permet une utilisation très flexible et une gestion des permissions très fines.

### 2. Intégrer notre code Ansible

➜  Par simplixité, je vous recommande de **créer un nouveau dépôt git** pour réaliser ce TP, afin de faciliter les interactions avec AWX. Hébergez-le publiquement.

Il contiendra :

- **`./Vagrantfile`**
  - il allume au moins une VM, qui utilise la box du TP3
    - juste à `vagrant up` pour avoir une machine prête à être utilisée avec Ansible
  - définissez une IP statique à la VM
- **tout le code Ansible du TP2**
  - à la racine aussi du dépôt
  - n'oubliez pas de modifier l'inventaire si nécessaire
  - précisez directement l'IP de la machine dans l'inventaire, pas un nom
    - cela facilitera l'interaction d'AWX (dans un conteneur) avec la VM

---

RDV maintenant sur l'interface Web d'AWX.

➜ **Créer un *Project***

- il pointe vers le dépôt git que vous venez de créer

➜ **Créer un *Credentials***

- pour déposer une identité avec : nom de user, password, clé SSH, comme on a l'habitude avec Ansible, **le type c'est Machine**
- indiquer le user et la clé privée que vous utilisez pour vous co à la VM
- normalement, c'est le user créé avec `cloud-init` au boot de la VM

➜ **Créer un *Inventaire***

- le fichier d'inventaire présent dans le dépôt git devrait remonter automatiquement

➜ **Créer un *Template***

- indiquer qu'il doit réutiliser toutes les entités créées précédemment
- vous devez aussi préciser quel playbook dérouler : ceux de votre dépôt git devraient remonter
- **run it !**
