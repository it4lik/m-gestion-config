# TP Gestion de Config

- [TP1 : Vagrant et Ansible](./1/README.md)
- [TP2 : Ansible et structure de dépôt](./2/README.md)
- [TP3 : First boot conf](./3/README.md)
- [TP4 : Prise en main AWX](./4/README.md)
- [TP5 : Conformité et CI/CD](./5/README.md)
- [TP6 : Your own stack](./6/README.md)

# Rendus de TP

Vous autre besoin d'un dépôt git pour effectuer les rendus de TP.

Tous les TPs devront être réalisés dans un unique dépôt Git.

Soyez curieux et avides pendant les TPs : n'hésitez pas à faire vos propres recherches et usez (abusez) de ma présence pour me poser des questions.

Avancez à votre rythme, le but est d'appréhender les concepts importants des technos qu'on voit en cours.
